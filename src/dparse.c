#include"include/dparse.h"




static uint8_t helper_allowedcountry(char** allowed, size_t len, char* countrystring){
    if(!len){
        return 1; // empty list means all countries are allowed
    }
    
    // always going through all countries is prob very slow, maybe construct trie?
    
    return 0; // disallowed
}

int handle_string(void *ctx, const uint8_t *val, size_t len){
    parse_t* s = (parse_t *)ctx;
    
    switch(s->state){
        case(STATE_URL):
            break;
        case(STATE_PROTOCOL):
            break;
        case(STATE_COUNTRYFULL):
            break;
        case(STATE_COUNTRYCODE):
            break;
        case(STATE_SKIP):
            break;
        default:
            break;
    }
    
    return 1;
}
