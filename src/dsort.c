#include"include/dsort.h"

static uint8_t tm_leq(struct tm* lhs, struct tm* rhs){
    if (lhs->tm_year < rhs->tm_year) return 1;
    if (lhs->tm_year > rhs->tm_year) return 0;

    if (lhs->tm_mon < rhs->tm_mon) return 1;
    if (lhs->tm_mon > rhs->tm_mon) return 0;

    if (lhs->tm_mday < rhs->tm_mday) return 1;
    if (lhs->tm_mday > rhs->tm_mday) return 0;
    
    if (lhs->tm_hour < rhs->tm_hour) return 1;
    if (lhs->tm_hour > rhs->tm_hour) return 0;
    
    if (lhs->tm_min < rhs->tm_min) return 1;
    if (lhs->tm_min > rhs->tm_min) return 0;
    
    if (lhs->tm_sec < rhs->tm_sec) return 1;
    if (lhs->tm_sec > rhs->tm_sec) return 0;
    
    return 1;
}

void dsort_init(dsort_t* s, size_t len){
    s->par = SPAR_NONE;
    s->cap = len;
    s->mirror = malloc(len*sizeof(s->cap));
    s->mirror_len = 0;
    return;
}

void sort(dsort_t* s, sortpar_e par){
    s->par = par;
    if(s->mirror_len){
        return;
    }
    
}

int dsort_insert(dsort_t* s, mirror_t* item){
    switch(s->par){
        case(SPAR_NONE):
            printf("error: inserting item into sorted array without specifying what to sort by\n");
            return -1;
        case(SPAR_FASTEST):
            for(size_t i=0; i<s->mirror_len; i++){ // if download duration of item is shorter than i'th item, shit all items and insert
               if(item->duration < s->mirror[i].duration){
                   for(size_t j=s->mirror_len-1; j>i; j--){
                       s->mirror[j] = s->mirror[j-1];
                   }
                   s->mirror[i] = *item;
                   return 0;
               }
            }
            break;
        case(SPAR_SCORE):
            for(size_t i=0; i<s->mirror_len; i++){ // if download duration of item is shorter than i'th item, shit all items and insert
               if(item->score > s->mirror[i].score){
                   for(size_t j=s->mirror_len-1; j>i; j--){
                       s->mirror[j] = s->mirror[j-1];
                   }
                   s->mirror[i] = *item;
                   return 0;
               }
            }
            break;
        case(SPAR_LATEST):
            for(size_t i=0; i<s->mirror_len; i++){ // if download duration of item is shorter than i'th item, shit all items and insert
               if(tm_leq(&s->mirror[i].lastupdate, &item->lastupdate)){
                   for(size_t j=s->mirror_len-1; j>i; j--){
                       s->mirror[j] = s->mirror[j-1];
                   }
                   s->mirror[i] = *item;
                   return 0;
               }
            }
            break;
        case(SPAR_DELAY):
            for(size_t i=0; i<s->mirror_len; i++){ // if download duration of item is shorter than i'th item, shit all items and insert
               if(item->delay < s->mirror[i].delay){
                   for(size_t j=s->mirror_len-1; j>i; j--){
                       s->mirror[j] = s->mirror[j-1];
                   }
                   s->mirror[i] = *item;
                   return 0;
               } else if((item->delay == s->mirror[i].delay) && (item->delay_dev < s->mirror[i].delay_dev)){
                   for(size_t j=s->mirror_len-1; j>i; j--){
                       s->mirror[j] = s->mirror[j-1];
                   }
                   s->mirror[i] = *item;
                   return 0;
               }
            }
            break;
        default:
            printf("error: unknown sort parameter %u\n", s->par);
            return -1;
    }
    if(s->mirror_len < s->cap){ // append to list if its not full
        s->mirror[s->mirror_len] = *item;
        s->mirror_len++;
    }
    return 0;
}


int dsort_topn(dsort_t* s, size_t num, mirror_t* buf){
    if(num < s->mirror_len){
        printf("warning: selecting top %lu from %lu mirrors\n", num, s->mirror_len);
        num = s->mirror_len;
        if(num < s->cap){
            printf(
                "error: selecting top %lu from %lu mirrors by fault of the parameters, please change your parameters\n",
                num, s->cap
            );
        }
    }
    mirror_t* ptr = malloc(num*sizeof(mirror_t));
    memcpy(ptr, s->mirror, num*sizeof(mirror_t));
    free(s->mirror);
    s->mirror = ptr;
    return 0;
}
