
#include"dutil.h"

#ifndef DPARSE_H
#define DPARSE_H


typedef enum {
    STATE_IDLE = 0,
    STATE_METAINFO = 1,
    STATE_URL = 2,
    STATE_DELAY = 3,
    STATE_DURATION = 4,
    STATE_SCORE = 5,
    STATE_ACTIVE = 6,
    STATE_COUNTRYFULL = 7,
    STATE_COUNTRYCODE = 8,
    STATE_IPV4 = 9,
    STATE_IPV6 = 10,
    STATE_PROTOCOL = 11,
    STATE_SKIP = 12,
} state_e;

typedef struct {
    uint32_t protocolmask;
    uint8_t ipvmask;
    char** countrmask;
    
    state_e state;
    mirror_t current;
} parse_t;


int handle_key(void *ctx, const uint8_t *val, size_t len);
int handle_string(void *ctx, const uint8_t *val, size_t len);


#endif