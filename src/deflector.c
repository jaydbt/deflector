#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>
#include<argp.h>

// YAJL to parse the mirrorlist json
#include<yajl/yajl_tree.h>
#include<yajl/yajl_parse.h>

// libcurl to download the mirrorlist and measure download speed
#include<curl/curl.h>

#include"include/dutil.h"

// TODO: cli arg parsing, sort/iterate mirrorlist, download timing function with writecb_mock

#define DEFLECTOR_VERSION "0.0.1"

#define DB_PATH "extra/os/x86_64/extra.db"
#define MIRRORLIST_TIMEOUT 5

const char MIRRORLIST_URL[] = "https://archlinux.org/mirrors/status/json/"; 


// mock write callback to measure speed
size_t writecb_mock(void *contents, size_t size, size_t nmemb, void *userp){
    return size*nmemb;
}

// write callback to write to a dynamic buffer
size_t writecb_dynbuf(void *contents, size_t size, size_t nmemb, void *userp){
    if(userp == NULL){
        return 0;
    }
    dynstr_t *buf = (dynstr_t *)userp;

    size_t wsize = size*nmemb;
    dynstr_append(buf, contents, wsize);
    
    return wsize;
}

static void list_countries(){
    printf("TODO\n");
}

static error_t parse_opt(int key, char *arg, struct argp_state *state){
    arg_t *args = state->input;
    
    switch(key){
        case(-1):
            args->connectiontimeout = atof(arg);
            break;
        case(-2):
            args->downloadtimeout = atof(arg);
            break;
        case(-3):
            list_countries();
            exit(0);
            break;
        case(-4):
            args->cachetimeout = atof(arg);
            break;
        case(-5):
            args->url = arg;
            break;
        case(-6):
            args->file = arg;
            break;
        case(-7):
            if(strcmp(arg, "age")){
                args->finalsort = SPAR_LATEST;
            } else if(strcmp(arg, "rate")){
                args->finalsort = SPAR_FASTEST;
            } else if(strcmp(arg, "country")){
                args->finalsort = SPAR_COUNTRY;
            } else if(strcmp(arg, "score")){
                args->finalsort = SPAR_SCORE;
            } else if(strcmp(arg, "delay")){
                args->finalsort = SPAR_DELAY;
            } else {
                printf("error: unknown sorting parameter %s", arg);
            }
            break;
        case(-8):
            args->numthreads = atol(arg);
            break;
        case(-9):
            args->verbose = 1;
            break;
        case(-10):
            args->info = 1;
            break;
        case(-11):
            args->delay = atof(arg);
            break;
        case(-12):
            args->completionpercent = atof(arg);
            break;
        case('a'):
            args->age = parse_datetime(arg);
            break;
        case('c'):
            args->countries = arg;
            break;
        default:
            if(arg == NULL){
                return 0;
            }
            printf("error: unknown command option %s\n", arg);
            exit(-1);
            break;
    }
    
    return 0;
}


int main(int argc, char** argv){
    int errc;
    
    const struct argp_option ao[] = { // TODO: add other flags
        {"connection-timeout", -1, "n", 0, " TODO "},
        {"download-timeout", -2, "n", 0, " TODO "},
        {"list-countries", -3, NULL, 0, " TODO "},
        {"cache-timeout", -4, "n", 0, " TODO "},
        {"url", -5, "URL", 0, " TODO "},
        {"save", -6, "<filepath>", 0, " TODO "},
        {"sort", -7, "{age,rate,country,score,delay}", 0, " TODO "},
        {"threads", -8, "n", 0, " TODO "},
        {"verbose", -9, NULL, 0, " TODO "},
        {"info", -10, NULL, 0, " TODO "},
    
        {"age", 'a', "n", 0, " TODO "},
        {"delay", -11, "n", 0, " TODO "},
        {"country", 'c', "<country name or code>", 0, " TODO "},
        {"completion-percent", -12, "[0-100]", 0, " TODO "},
        
        {0},
    };
    
    struct argp _argp = {ao, parse_opt, 0, 0};
    
    arg_t defaults = {
        5,
        5,
        5,
        0,
        100.0,
        {0,0,0,0,0,0,0,0,0,0,0}, //TODO
        1,
        0,
        0,
        0,
        &MIRRORLIST_URL[0],
        0,
        0,
        SPAR_NONE,
        &_argp,
    };
    
    
    errc = argp_parse(&_argp, argc, argv, 0, 0, &defaults);
    if(errc != 0){
        printf("couldnt parse arguments supplied\n");
        exit(-1);
    }
    
    CURLcode curlerr;
    
    
    char errbuf[CURL_ERROR_SIZE];

    const char mirrorlist_url[] = "https://archlinux.org/mirrors/status/json/";

    CURL *ehandle = curl_easy_init();

    if(!ehandle){
        fprintf(stderr, "failed to initialize libcurl\n");
        exit(-1);
    }

    dynstr_t mirrorlist;
    dynstr_init(&mirrorlist);
    
    
    curlerr = curl_easy_setopt(ehandle, CURLOPT_URL, mirrorlist_url);
    handle_curlerr(curlerr, NULL);
    curlerr = curl_easy_setopt(ehandle, CURLOPT_FOLLOWLOCATION, 1L);
    handle_curlerr(curlerr, NULL);
    
    curlerr = curl_easy_setopt(ehandle, CURLOPT_WRITEFUNCTION, writecb_dynbuf);
    handle_curlerr(curlerr, NULL);
    curlerr = curl_easy_setopt(ehandle, CURLOPT_WRITEDATA, &mirrorlist);
    handle_curlerr(curlerr, NULL);
    
    curlerr = curl_easy_setopt(ehandle, CURLOPT_ERRORBUFFER, &errbuf);
    handle_curlerr(curlerr, NULL);
    curlerr = curl_easy_setopt(ehandle, CURLOPT_TIMEOUT, MIRRORLIST_TIMEOUT);
    handle_curlerr(curlerr, NULL);
    
    curlerr = curl_easy_perform(ehandle);
    handle_curlerr(curlerr, errbuf);
    
    

    curl_easy_cleanup(ehandle);
    return 0;
}

