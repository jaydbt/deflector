#include"include/dutil.h"

// -------------------------------------------------------------------

// handle errors from libcurl
inline void handle_curlerr(CURLcode errc, char* errbuf){
    if(errc != 0){
        printf("encountered curl-error [%i]\n", errc);
        if(errbuf != NULL){
            printf("[%s]", errbuf);
        }
        exit(-1);
    }
}

// handle errors from libyajl
inline void handle_yajlerr(int errc){
    if(errc != 0){
        printf("encountered yajl-error [%i]\n", errc);
    }
}

// -------------------------------------------------------------------

// initialize dynamic string
void dynstr_init(dynstr_t *buf){
    buf->len = 0;
    buf->ptr = malloc(1*sizeof(uint8_t));
}

// append to the dynamic string
void dynstr_append(dynstr_t *buf, void* data, size_t len){
    buf->ptr = realloc(buf->ptr, buf->len+len+1);
    if(buf->ptr == NULL){
        exit(-1);
    }
    memcpy(&(buf->ptr[buf->len]), data, len);
    buf->len += len;
    ((uint8_t *)buf->ptr)[buf->len] = '\0';
}

// -------------------------------------------------------------------

struct tm parse_datetime(const char* input){
    struct tm datetime;
    char* ret = strptime(input, "%Y-%m-%dT%H:%M:%SZ", &datetime);
    if(*ret != '\0'){
        printf("error: unexpected date-time format\n");
        exit(-1);
    }
    return datetime;
}




