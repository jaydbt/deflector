# Deflector

## WORK IN PROGRESS, NOT CLOSE TO BEING FUNCTIONAL AT THE MOMENT

## Description
Deflector is a replacement for the reflector package, used to update the mirrorlist for pacman.
It mimics reflectors cli.

## TODO
- [ ] Work as a drop in replacement for reflector
- [ ] Multithreading
- [ ] Cache mirrorlist
- [ ] Replace dependency on YAJL?

