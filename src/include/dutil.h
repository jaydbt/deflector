#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>
#include<argp.h>
// YAJL to parse the mirrorlist json
#include<yajl/yajl_tree.h>
#include<yajl/yajl_parse.h>
// libcurl to download the mirrorlist and measure download speed
#include<curl/curl.h>
// time stamp parsing
#include<time.h>

#ifndef DUTIL_H
#define DUTIL_H

typedef enum {
    SPAR_NONE = 0,
    SPAR_FASTEST = 1,
    SPAR_LATEST = 2,
    SPAR_SCORE = 3,
    SPAR_DELAY = 4,
    SPAR_COUNTRY = 5,
} sortpar_e;

void handle_curlerr(CURLcode errc, char* errbuf);
void handle_yajlerr(int errc);


typedef struct {
    void *ptr;
    size_t len;
} dynstr_t;

void dynstr_init(dynstr_t *buf);
void dynstr_append(dynstr_t *buf, void* data, size_t len);
void dynstr_free(dynstr_t *buf);


struct tm parse_datetime(const char* input);

typedef struct {
    char* url;
    float score;
    float delay;
    float delay_dev;
    float duration;
    float completion;
    struct tm lastupdate;
    
    float downloadtime;
} mirror_t;


typedef struct {
    float connectiontimeout;
    float downloadtimeout;
    float cachetimeout;
    float delay;
    float completionpercent;
    
    struct tm age;
    
    size_t numthreads;
    
    uint8_t listcountries;
    uint8_t verbose;
    uint8_t info;
    
    char *url;
    char *file;
    char *countries;
    
    sortpar_e finalsort;
    
    struct argp* argp_def;
} arg_t;

#endif