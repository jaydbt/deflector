

#ifndef DSORT_H
#define DSORT_H
#include"dutil.h"

typedef struct {
    uint16_t lhs; // if zero the node is a leaf and the rhs points to the data array entry of the leaf
    uint16_t rhs;
} dnode_t;

// holds information on how to sort and sorted tree structure
typedef struct {
    sortpar_e par;
    size_t cap;

    mirror_t* mirror;
    size_t mirror_len;    
} dsort_t;



void dsort_init(dsort_t* s, size_t len);
void dsort_reset(dsort_t* s);
int dsort_insert(dsort_t* s, mirror_t* item);
int dsort_topn(dsort_t* s, size_t num, mirror_t* buf);


#endif